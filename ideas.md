# Large
- resume
  - make old proj's displayable
    - spv
      - screenshots
      - setup instructions
    - see if SoundCloud API still bad (TrapBot)
    - ADA vid/pics
    - lc setup
    - image tagger?
      - executable
  - date on same line as project
  - collapsible sections
    - link to each section beside it
  - fix title
  - collapsible sections
    - more
  - printable version
    - JS to change CSS
  - list of things done with diff. langs.
- add CSS grid layout
  - add tooltip saying responsive 
- articles (ideas)
- screenshots gallery
  - infinite scroll

- loop through URL sections
- ask about most efficient way for custom layout for each section
- JS snippets?
- rewrite CSS in Sass
  - push to base16 repo
- terminal simulator for site
  - cd/ls diff. pages

# Medium
- socials in footer
  - icons
  - email
    - tooltip pop-up with email and clipboard button/link
    - [Pure CSS Tooltips](https://www.youtube.com/watch?v=hAeQ8CqrGDY)
    - use position element
    - [Copying Text to Clipboard in HTML & JavaScript](https://www.youtube.com/watch?v=NHg6jQajaMs)
    - [Native Browser Copy To Clipboard](https://css-tricks.com/native-browser-copy-clipboard/)
- pages
  - fav. apps
  - linux config 
  - credits
  - contact?
- site stats
  - size
  - load time

# Tiny
- SEO
  - add/redirect aliases
  - be more specific with robots.txt when have content
- create different size images for SVG
  - https://www.sitepoint.com/svg-good-for-website-performance/ 

Break> {{{
> }}}

## Archived
- about/homepage
  - typing animation
  - linux screenshot
    - fade between two
- tux icon
