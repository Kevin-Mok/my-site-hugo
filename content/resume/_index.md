---
title: "Resume"
date: 2019-02-11T07:50:51-05:00
draft: false
---
{{% resume/section goal %}}<!--- {{{ -->

To find an internship to fully dedicate myself that will test my limits,
provide the opportunity to gain valuable knowledge and experience and
jumpstart my career with a solid foundation.

<!--- }}} -->

{{% resume/section projects %}}<!--- {{{ -->

<!--- Spotify Graphs {{{ -->

{{< resume/project-header name="Spotify Visualizer" url="https://github.com/Kevin-Mok/spotify-lib-vis" date="July 2018" >}}

{{% resume/project-details %}}

- Web app that scans your Spotify library using their API to produce [d3
  graphs][d3 graph examples] to visualize the artists, genres and features
  of tracks in your library.
- Can also keep track of your listening history. Currently runs as a `cron`
  job on my local machine to keep track of my own.
- Uses Django and PostgreSQL for back-end.

[d3 graph examples]: https://github.com/d3/d3/wiki/Gallery

{{% /resume/project-details %}}

<!--- Spotify Graphs }}} -->

<!--- ParsaFood {{{ -->

{{< resume/project-header name="ParsaFood" url="https://github.com/Kevin-Mok/ParsaFood" date="Feb. 2018" >}}

{{% resume/project-details %}}
- Android app that reads ingredients labels and detects any dietary			
  restrictions/allergies. Came up with the idea based on 
  [personal experience][Parsafood motivation].
- Made during the 24-hour [Platterz	Hackathon][Platterz Hackathon event] in
  a group with two other members. Came in 2nd place and won an 
  [Oculus Rift][Oculus Rift Amazon].

[Parsafood repo]: https://github.com/Kevin-Mok/ParsaFood
[Parsafood motivation]: https://github.com/Kevin-Mok/ParsaFood#motivation
[Platterz Hackathon event]: https://www.eventbrite.com/e/platterz-hackathon-tickets-42682237722
[Oculus Rift Amazon]: https://www.amazon.ca/Oculus-Rift-Windows-VR-Headset/dp/B00VF0IXEY/ref=sr_1_1_sspa?ie=UTF8&qid=1520422949&sr=8-1-spons&keywords=oculus+rift&psc=1
{{% /resume/project-details %}}

<!--- ParsaFood }}} -->
 
<!--- Grocery Finder {{{ -->

{{< resume/project-header name="Grocery Finder" url="https://github.com/Kevin-Mok/grocery-finder" date="April 2019" >}}

{{% resume/project-details %}}
- Proof of concept for a web app that finds the best supermarket for your needs.
- Took on role as project leader to prioritize and delegate tasks while
  also implementing a majority of the core features.
- Responsive front-end using Bootstrap. Uses Node.js and MongoDB for back-end.
- Currently deployed on [Heroku][Grocery Finder Heroku].

[Grocery Finder Heroku]: http://www.grocery-finder.ml

{{% /resume/project-details %}}


<!--- Grocery Finder }}} -->

{{% /resume/section %}}<!--- }}} -->

{{% resume/section education %}}<!--- {{{ -->

- **University of Toronto**: Computer Science Specialist <div style="float:right">2017-2021</div>

{{% /resume/section %}}<!--- }}} -->
